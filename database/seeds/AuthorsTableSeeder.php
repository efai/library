<?php

use Illuminate\Database\Seeder;
use \App\Models\Author;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert authors (static data)
        $authors = [
            'Joseph Heller',
            'Jozef Karika',
            'Mario Puzo',
            'J.R.R. Tolkien'
        ];

        foreach ($authors as $author) {
            Author::updateOrCreate(['name' => $author]);
        }
    }
}