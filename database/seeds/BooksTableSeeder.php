<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // check if table books is empty
        if (DB::table('books')->get()->count() == 0) {
            // Insert books (static data)
            DB::table('books')->insert([
                ['title' => 'Hlava 22', 'isbn' => '9780099529118', 'price' => '12.30', 'category_id' => 2, 'author_id' => 1],
                ['title' => 'Čierny rok: Vojna mafie', 'isbn' => '9788055157528', 'price' => '118.01', 'category_id' => 3, 'author_id' => 2],
                ['title' => 'Krstný otec', 'isbn' => '9788055146461', 'price' => '66.95', 'category_id' => 3, 'author_id' => 4],
                ['title' => 'Pán prsteňov I. - Spoločenstvo prsteňa', 'isbn' => '9788055606347', 'price' => '16.15', 'category_id' => 1, 'author_id' => 3]
            ]);
        }
    }
}