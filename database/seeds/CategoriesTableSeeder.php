<?php

use Illuminate\Database\Seeder;
use \App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert categories (static data)
        $categories = [
            'Fantasy',
            'Román',
            'Triler'
        ];

        foreach ($categories as $category) {
            Category::updateOrCreate(['cat_name' => $category]);
        }
    }
}