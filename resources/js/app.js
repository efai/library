require('./bootstrap');

$(function() {
    $('#author').autocomplete({
        source: function(request, response) {
            $.ajax({
            url: '/search',
            data: {
                term : request.term
            },
            dataType: 'json',
            success: function(data) {
                let resp = $.map(data, function(obj) {
                    return obj.name;
                }); 
               response(resp);
            }
        });
    },
    minLength: 2
    });
});