<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Knižnica') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Autocomplete Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Open Sans" rel="stylesheet">
    <!-- Autocomplete Style -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">

@if (session()->has('store'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('store') }}
    </div>
@endif

{{--  display errors  --}}
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif

        <div class="container col-12 mt-5 col-sm-6 offset-sm-3">
            <h1 class="display-4 font-weight-bold text-center"> Knižnica </h1>
        @if(\DB::getSchemaBuilder()->hasTable('books'))
            <h6 class="text-center"> <a href="{{ route('json') }}">JSON</a> </h6>
        @endif

            <form action="" method="post">
                @csrf

                <div class="form-group">
                    <input type="text" class="form-control" id="title" name="title" placeholder="Názov knihy" value="{{ old('title') }}">
                </div>

                <div class="form-row justify-content-between">
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="isbn" name="isbn" placeholder="ISBN" value="{{ old('isbn') }}">
                    </div>

                    <div class="col-sm-5 mt-3 mt-sm-0">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">$</span>
                            </div>
                            <input type="text" class="form-control" id="price" name="price" placeholder="Cena"
                            aria-label="Euro amount (with dot or comma and two decimal places)" value="{{ old('price') }}">
                        </div>
                    </div>
                </div>

                <div class="form-row mt-0 mt-sm-0 justify-content-between">
                    <div class="col-12 col-sm-5">
                        <select class="form-control" id="category" name="category_id">
                        @forelse($categories as $k => $c)
                            <option value="{{ $k }}"{{ old('category_id') == $k ? ' selected' : '' }}>{{ $c }}</option>
                        @empty
                            <option>-- Run migrate+seed to populate the options --</option>
                        @endforelse
                        </select>
                    </div>

                    <div class="col-sm-5 mt-3 mt-sm-0">
                        <input type="text" class="form-control" id="author" name="author" placeholder="Autor" value="{{ old('author') }}">
                    </div>
                </div>

                <div class="col-12 col-sm-5 mt-3 px-0 offset-sm-7">
                    <button type="submit" class="btn btn-success ml-sm-1 col-sm-12"@if(!\DB::getSchemaBuilder()->hasTable('books')){{ ' disabled' }}@endif>
                        Pridať knihu do knižnice
                    </button>
                </div>
            </form>

            <hr class="my-5" />

            <table class="table table-bordered table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Názov knihy</th>
                        <th scope="col" class="d-none d-sm-table-cell">ISBN</th>
                        <th scope="col">Cena
                        @if(request()->path() === '/')
                            <a href="{{ route('sort-by-price', 'price-asc') }}">
                                <img class="ml-2 bg-white" src="/images/sort.png" width="16" height="16" alt="sort" title="sort"/>
                            </a>
                        @else
                            <a href="{{ route('index') }}">
                                <img class="ml-2 bg-white" src="/images/sort_up.png" width="16" height="16" alt="sort" title="sort"/>
                            </a>
                        @endif
                        </th>
                        <th scope="col" class="d-none d-sm-table-cell">Kategória</th>
                        <th scope="col">Autor</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($books as $b)
                    <tr>
                        <td>{{ $b->title }}</th>
                        <td class="d-none d-sm-table-cell">{{ $b->isbn }}</td>
                        <td>{{ number_format($b->price, 2, '.', ' ') }}€</td>
                        <td class="d-none d-sm-table-cell">{{ $categories[$b->category_id] }}</td>
                        <td>{{ $authors[$b->author_id] }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">Bez záznamu</th>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>
