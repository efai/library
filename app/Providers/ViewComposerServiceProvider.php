<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Author;
use App\Models\Category;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeSelect();
        $this->composeAuthors();
    }

    private function composeSelect()
    {
        view()->composer(['index'], function($view) {
            if (\DB::getSchemaBuilder()->hasTable('categories')) {
                $categories = Category::pluck('cat_name', 'id');
            } else {
                $categories = [];
            }

            $view->with(compact('categories'));
        });
    }

    private function composeAuthors()
    {
        view()->composer(['index'], function($view) {
            if (\DB::getSchemaBuilder()->hasTable('authors')) {
                $authors = Author::pluck('name', 'id');
            } else {
                $authors = [];
            }

            $view->with(compact('authors'));
        });
    }
}