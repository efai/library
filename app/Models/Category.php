<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['cat_name'];

    public $timestamps = false;

    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}