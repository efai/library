<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Author;

class Book extends Model
{
    protected $fillable = [
        'title', 'isbn', 'price', 'category_id', 'author_id'
    ];

    public $timestamps = false;

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function author()
    {
        return $this->hasOne(Author::class, 'id', 'author_id');
    }

    public function setPriceAttribute($price)
    {
        if (!is_null($price)) {
            $this->attributes['price'] = str_replace(',', '.', request()->input('price'));
        }
    }

    public function handleAuthor($author_name)
    {
        $author = Author::firstOrCreate(['name' => $author_name]);
        return $author->id;
    }
}