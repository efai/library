<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = ['name'];

    public $timestamps = false;

    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}