<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Author;

class AuthorController extends Controller
{
    public function search(Request $request)
    {
        $search = $request->get('term');
        $result = Author::select('name')->where('name', 'LIKE', '%'. $search. '%')->get();

        return response()->json($result);
    }
}