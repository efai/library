<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkCommaDot');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($paArg = null)
    {
        if (is_null($paArg)) {
            if (\DB::getSchemaBuilder()->hasTable('books')) {
                $books = Book::all();
            } else {
                $books = [];
            }
        } else {
            $books = Book::oldest('price')->get();
        }

        return view('index', compact('books'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Book $book)
    {
        $data = $request->validate([
            'title' => 'required|string|unique:books|max:64',
            'isbn' => 'required|string|max:24',
            'price' => 'required|numeric|between:0,199.99',
            'category_id' => 'required|integer',
            'author' => 'required|string|max:32'
        ],
        [
            'title.required' => 'Názov knihy je povinný',
            'title.unique' => 'Názov knihy už existuje',
            'title.max' => 'Názov knihy nesmie byť dlhší ako 64 znakov',
            'isbn.required' => 'ISBN je povinný',
            'isbn.max' => 'ISBN nesmie byť dlhší ako 24 znakov',
            'price.required' => 'Cena je povinná',
            'price.between' => 'Hodnota ceny nie je v rozmedzí :min - :max',
            'category_id.required' => 'Kategória je povinná',
            'author.required' => 'Autor je povinný',
            'author.max' => 'Autor nesmie byť dlhší ako 32 znakov',
        ]);

        $data['author_id'] = $book->handleAuthor($data['author']);
        unset($data['author']);

        $b = $book->create($data);

        return redirect()->back()->with('store', 'Kniha ' . $b->title . ' bola úspešne pridaná do knižnice');
    }
}