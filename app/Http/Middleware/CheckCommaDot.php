<?php

namespace App\Http\Middleware;

use Closure;

class CheckCommaDot
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request['price']) {
            $request['price'] = str_replace(',', '.', $request['price']);
        }

        return $next($request);
    }
}